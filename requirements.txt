https://download.pytorch.org/whl/cu100/torch-1.0.1.post2-cp35-cp35m-linux_x86_64.whl
absl-py==0.8.0
astor==0.8.0
cycler==0.10.0
gast==0.2.2
grpcio==1.13.0
grpcio-tools==1.13.0
Markdown==3.1.1
matplotlib==2.1.0
numpy==1.15.4
Pillow==6.1.0
protobuf==3.5.1
pyparsing==2.4.2
python-dateutil==2.8.0
python-Levenshtein==0.12.0
pytz==2019.2
six==1.12.0
tensorboard==1.9.0
tensorboardX==1.4
tensorflow==1.9.0
termcolor==1.1.0
torchvision==0.2.2.post3
Unidecode==1.0.22
Werkzeug==0.15.6