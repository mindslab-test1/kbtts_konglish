#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import logging
import time
import torch
import grpc
import argparse
import signal

from concurrent import futures
from grpc_health.v1 import health
from grpc_health.v1 import health_pb2_grpc

from data import english_cleaners, sequence_to_kor
from train import load_model
from konglish_pb2_grpc import add_KonglishServicer_to_server, KonglishServicer
from konglish_pb2 import Korean

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def preprocess(word_list):
    word_list = [torch.cuda.LongTensor(english_cleaners(text)) for text in word_list]
    input_lengths, ids_sorted_decreasing = torch.sort(
        torch.cuda.LongTensor([len(text) for text in word_list]),
        dim=0, descending=True)
    max_input_len = input_lengths[0]

    input_padded = torch.cuda.LongTensor(len(word_list), max_input_len)
    input_padded.zero_()
    for i in range(len(ids_sorted_decreasing)):
        text = word_list[ids_sorted_decreasing[i]]
        input_padded[i, :text.size(0)] = text
    output_ids = torch.sort(ids_sorted_decreasing)[1]
    return (input_padded, input_lengths), output_ids


class KonglishServicerImpl(KonglishServicer):
    def __init__(self, checkpoint_path, device, max_decoder_steps):
        super().__init__()
        torch.cuda.set_device(device)
        self.device = device

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        hparams = checkpoint['hparams']
        hparams.max_decoder_steps = max_decoder_steps

        state_dict = checkpoint['state_dict']

        self.model = load_model(hparams)
        self.model.load_state_dict(state_dict)
        self.model.eval()

        del hparams
        del state_dict
        del checkpoint
        torch.cuda.empty_cache()

    def Transliterate(self, request, context):
        try:
            torch.cuda.set_device(self.device)
            with torch.no_grad():
                english = request.words
                logging.debug('request:%s', english)
                english, output_ids = preprocess(english)
                korean, _ = self.model.inference(*english)
                korean = [sequence_to_kor(korean[idx.item()]) for idx in output_ids]
                logging.debug('response:%s', korean)
                return Korean(words=korean)
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='konglish runner executor')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Path.',
                        type=str)
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=20001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)
    parser.add_argument('-s', '--max_decoder_steps',
                        nargs='?',
                        dest='max_decoder_steps',
                        help='korean jamo length limit',
                        type=int,
                        default=100)

    args = parser.parse_args()

    konglish_servicer = KonglishServicerImpl(args.model, args.device, args.max_decoder_steps)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1),)
    add_KonglishServicer_to_server(konglish_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    health_servicer = health.HealthServicer()
    health_pb2_grpc.add_HealthServicer_to_server(health_servicer, server)
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    def exit_gracefully(signum, frame):
        health_servicer.enter_graceful_shutdown()
        server.stop(60)
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    logging.info('konglish starting at 0.0.0.0:%d', args.port)

    server.wait_for_termination()
