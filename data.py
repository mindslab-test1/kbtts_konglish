import re
import random
import torch.utils.data
import Levenshtein as Lev

from unicodedata import normalize

_sos = '<s>'
_eos = '</s>'
_pad = '<pad>'

_punc = ' '

_eng_characters = 'abcdefghijklmnopqrstuvwxyz'

_jamo_leads = "".join([chr(_) for _ in range(0x1100, 0x1113)])
_jamo_vowels = "".join([chr(_) for _ in range(0x1161, 0x1176)])
_jamo_tails = "".join([chr(_) for _ in range(0x11A8, 0x11C3)])
_kor_characters = _jamo_leads + _jamo_vowels + _jamo_tails

eng_symbols = [_pad, _sos, _eos, "'", "-"] + list(_punc) + list(_eng_characters)
kor_symbols = [_pad, _sos, _eos, "-"] + list(_punc) + list(_kor_characters)

eng_to_id = {s: i for i, s in enumerate(eng_symbols)}
id_to_eng = {i: s for i, s in enumerate(eng_symbols)}

kor_to_id = {s: i for i, s in enumerate(kor_symbols)}
id_to_kor = {i: s for i, s in enumerate(kor_symbols)}


_whitespace_re = re.compile(r'\s+')

kor_symbol_len_list = [5, len(_jamo_leads), len(_jamo_vowels), len(_jamo_tails)]
kor_transition_rule = [(0, 1,), (2,), (3, 0, 1,), (0, 1,)]
kor_symbol_boundary = []
_start = 0
for kor_symbol_len in kor_symbol_len_list:
    _end = _start + kor_symbol_len
    kor_symbol_boundary.append((_start, _end))
    _start = _end


def lowercase(text):
    return text.lower()


def collapse_whitespace(text):
    return re.sub(_whitespace_re, ' ', text)


def english_cleaners(text):
    text = lowercase(text)
    text = collapse_whitespace(text)
    sequence = [eng_to_id[symbol] for symbol in text if symbol in eng_to_id]
    sequence.append(eng_to_id[_eos])
    return sequence


def korean_cleaners(text):
    text = collapse_whitespace(text)
    text = normalize('NFKD', text)
    sequence = [kor_to_id[symbol] for symbol in text if symbol in kor_to_id]
    sequence.append(kor_to_id[_eos])
    return sequence


def sequence_to_eng(sequence):
    target_text = []
    for c in sequence:
        c = c.item()
        if c == 2:
            break
        elif c < 2:
            continue
        target_text.append(id_to_eng[c])
    target_text = ''.join(target_text)
    return target_text


def sequence_to_kor(sequence):
    target_text = []
    for c in sequence:
        c = c.item()
        if c == 2:
            break
        elif c < 2:
            continue
        target_text.append(id_to_kor[c])
    target_text = ''.join(target_text)
    target_text = normalize('NFKC', target_text)
    return target_text


def calculate_ler(prediction, target):
    """
    Computes the Character Error Rate, defined as the edit distance.
    Arguments:
        prediction (string): space-separated sentence
        target (string): space-separated sentence
    """
    prediction = normalize('NFKD', prediction)
    target = normalize('NFKD', target)
    return Lev.distance(prediction, target), len(target)


class TransliteratorDataset(torch.utils.data.Dataset):
    def __init__(self, txt_file, hparams):
        self.data_list = []
        with open(txt_file, 'r', encoding='utf-8') as rf:
            for line in rf.readlines():
                data = line.replace('\n', '').split('\t')
                eng = data[0]
                kor = data[1]
                self.data_list.append((eng, kor))
        random.seed(hparams.seed)
        random.shuffle(self.data_list)

    def __getitem__(self, index):
        eng, kor = self.data_list[index]

        eng = english_cleaners(eng)
        eng = torch.IntTensor(eng)

        kor = korean_cleaners(kor)
        kor = torch.IntTensor(kor)

        return eng, kor

    def __len__(self):
        return len(self.data_list)


class TextCollate:
    def __call__(self, batch):
        input_lengths, ids_sorted_decreasing = torch.sort(
            torch.LongTensor([len(x[0]) for x in batch]),
            dim=0, descending=True)
        max_input_len = input_lengths[0]

        input_padded = torch.LongTensor(len(batch), max_input_len)
        input_padded.zero_()
        for i in range(len(ids_sorted_decreasing)):
            text = batch[ids_sorted_decreasing[i]][0]
            input_padded[i, :text.size(0)] = text

        max_output_len = max([x[1].size(0) for x in batch])

        output_padded = torch.LongTensor(len(batch), max_output_len)
        output_padded.zero_()
        output_lengths = torch.LongTensor(len(batch))
        for i in range(len(ids_sorted_decreasing)):
            text = batch[ids_sorted_decreasing[i]][1]
            output_padded[i, :text.size(0)] = text
            output_lengths[i] = text.size(0)

        return input_padded, input_lengths, output_padded, output_lengths
