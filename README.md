# brain-konglish
영어 단어를 한글 표기로 변환하는 엔진

## Setup
1. `pip install -r requirements.txt`
1. `python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. konglish.proto`

## Train
1. Download [training_data](https://pms.maum.ai/confluence/download/attachments/15336273/training_data.txt?version=1&modificationDate=1567737682624&api=v2), [validation_data](https://pms.maum.ai/confluence/download/attachments/15336273/validation_data.txt?version=1&modificationDate=1567737695839&api=v2) 
1. `python train.py`

## Run Server
1. Download [model](https://pms.maum.ai/confluence/download/attachments/15336277/model_v1.pt?version=1&modificationDate=1567737724682&api=v2)
1. `python server.py -m model.pt`

## Run Client
1. `python client.py`
