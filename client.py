#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import argparse
import grpc

from konglish_pb2_grpc import KonglishStub
from konglish_pb2 import English


class KonglishClient(object):
    def __init__(self, remote='127.0.0.1:20001'):
        channel = grpc.insecure_channel(remote)
        self.stub = KonglishStub(channel)

    def transliterate(self, word_list):
        return self.stub.Transliterate(English(words=word_list))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='konglish client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:20001')
    parser.add_argument('-i', '--input',
                        nargs='+',
                        dest='input',
                        help='input english',
                        type=str,
                        default=['minds', 'lab'])
    args = parser.parse_args()

    client = KonglishClient(args.remote)

    korean = client.transliterate(args.input)
    print(korean.words)
