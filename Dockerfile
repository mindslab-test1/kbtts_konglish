FROM nvcr.io/nvidia/pytorch:19.06-py3

RUN mkdir /root/konglish

COPY . /root/konglish

RUN python3 -m pip --no-cache-dir install --upgrade \
        tensorflow==1.9.0 \
        tensorboardX==1.2 \
        grpcio==1.38.0 \
        grpcio-tools==1.38.0 \
        grpcio-health-checking==1.38.0 \
        protobuf==3.15.8 \
        python-Levenshtein==0.12.0 && \
    cd /root/konglish &&\
    python3 -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. konglish.proto && \
# ==================================================================
# config & cleanup
# ------------------------------------------------------------------
    ldconfig && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /workspace/*
